package repository.persistent;

import domain.data.source.database.MetaDB;
import domain.data.source.database.table.ChatTable;
import domain.model.Chat;
import org.jetbrains.annotations.NotNull;
import repository.memory.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public class ChatDBRRepository extends Repository<Integer, Chat> {

    private final ChatTable table;

    public ChatDBRRepository() throws SQLException {
        this.table = (ChatTable) MetaDB.getInstance().table("chats");
        for (final Chat chat : this.table.getAll()) {
            super.add(chat);
        }
    }

    @Override
    public @NotNull Optional<Chat> add(@NotNull final Chat chat) throws SQLException {
        final Optional<Chat> result = super.add(chat);
        if (result.isPresent()) {
            this.table.insert(chat);
        }
        return result;
    }

    @Override
    public @NotNull Optional<Chat> update(@NotNull final Chat chat) throws SQLException {
        final Optional<Chat> result = super.update(chat);
        if (result.isPresent()) {
            this.table.update(chat);
        }
        return result;
    }

    @Override
    public @NotNull Optional<Chat> findByID(@NotNull final Integer id) throws SQLException {
        return this.table.whereID(id);
    }

    @Override
    public @NotNull Optional<Chat> delete(@NotNull final Integer id) throws SQLException {
        final Optional<Chat> result = super.delete(id);
        if (result.isPresent()) {
            this.table.delete(id);
        }
        return result;
    }

    @Override
    public @NotNull ArrayList<Chat> getAll() throws SQLException {
        return this.table.getAll();
    }
}
