package repository.persistent;

import domain.data.source.database.MetaDB;
import domain.data.source.database.table.MessageTable;
import domain.model.Message;
import org.jetbrains.annotations.NotNull;
import repository.memory.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public class MessageDBRepository extends Repository<Integer, Message> {

    private final MessageTable table;

    public MessageDBRepository() {
        this.table = (MessageTable) MetaDB.getInstance().table("messages");
    }

    @Override
    public @NotNull Optional<Message> add(@NotNull final Message message) throws SQLException {
        final Optional<Message> result = super.add(message);
        if (result.isPresent()) {
            this.table.insert(message);
        }
        return result;
    }

    @Override
    public @NotNull Optional<Message> update(@NotNull final Message message) throws SQLException {
        final Optional<Message> result = super.update(message);
        if (result.isPresent()) {
            this.table.update(message);
        }
        return result;
    }

    @Override
    public @NotNull Optional<Message> findByID(@NotNull final Integer id) throws SQLException {
        return this.table.whereID(id);
    }

    @Override
    public @NotNull Optional<Message> delete(@NotNull final Integer id) throws SQLException {
        final Optional<Message> result = super.delete(id);
        if (result.isPresent()) {
            this.table.delete(id);
        }
        return result;
    }

    @Override
    public @NotNull ArrayList<Message> getAll() throws SQLException {
        return this.table.getAll();
    }
}
