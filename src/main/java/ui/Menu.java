package ui;

import controller.ChatController;
import controller.FriendshipController;
import domain.model.Chat;
import domain.model.Friendship;
import domain.model.Tuple;
import domain.model.Message;
import domain.model.User;
import domain.model.*;
import domain.validation.FriendshipValidator;
import domain.validation.UserValidator;
import org.jetbrains.annotations.NotNull;
import repository.Repository;
import repository.persistent.*;
import service.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class Menu {

    private UserService userService;
    private ChatService chatService;
    private RequestService requestService;
    private MessageService messageService;
    private FriendshipController controller;
    private ChatController chatController;
    private BufferedReader reader;

    public Menu() {
        try {
            // repos
            final Repository<Long, User> userRepository
                    = new UserDBRepository();
            final Repository<Long, Friendship> friendShipRepository
                    = new FriendshipDBRepository(userRepository::getAll);
            final Repository<Integer, Chat> chatRepository
                    = new ChatDBRRepository();
            final Repository<Integer, Message> messageRepository
                    = new MessageDBRepository();
            final Repository<Integer, FriendshipRequest> requestRepository
                    = new RequestRepository();
            // validators
            final UserValidator userValidator = new UserValidator();
            final FriendshipValidator friendshipValidator = new FriendshipValidator();

            // services
            this.userService = new UserService(userValidator, userRepository);
            this.chatService = new ChatService(chatRepository);
            this.messageService = new MessageService(messageRepository);
            final FriendshipService friendShipService
                    = new FriendshipService(friendshipValidator, friendShipRepository);
            this.requestService = new RequestService(requestRepository);

            // controllers
            this.controller = new FriendshipController(userService, friendShipService);
            this.chatController = new ChatController(this.chatService, this.messageService);

            // reader
            this.reader = new BufferedReader(new InputStreamReader(System.in));
        } catch (Exception exception) {
            System.out.println("\n\n" + exception.getMessage() + "\n\n");
            System.exit(1);
        }
    }

    public final void start() {
        this.displayAvailableCommands();
        this.startGetUserInput();
    }

    private void displayAvailableCommands() {
        System.out.println("""
                'help' -> display this set of all available commands
                'add -u/f/ch/m/fr' -> add user/friendship/chat/message/request
                'find -u/f/ch/m' -> find user/friendship/chat/message by id
                'up -u/ch/m' -> update user/chat/message
                'del -u/f/ch/m' -> delete user/friendship/chat/message
                'all -u/f/c/ch/m/fr' -> display all users/friendships/communities/chats/messages/request
                'all -cN' -> display communities number
                'comm -a' -> display the most active community
                'friendsofauser' -> displays a user's friends
                'friendsofauserm' -> displays a user's friends for a month
                """);
    }

    private void startGetUserInput() {
        final AtomicInteger exceptionCounter = new AtomicInteger(0);
        while (true) {
            try {
                System.out.print(">>>");
                final String command = reader.readLine();
                if (command.isEmpty()) {
                    continue;
                }

                switch (command) {
                    case "exit" -> {
                        System.out.println("exiting meta-console...");
                        return;
                    }
                    case "help" -> this.displayAvailableCommands();
                    case "add -u" -> this.addUser();
                    case "add -f" -> this.addFriendship();
                    case "add -ch" -> this.addChat();
                    case "add -m" -> this.addMessage();
                    case "add -fr" -> this.addRequest();
                    case "find -u" -> this.findUserByID();
                    case "find -f" -> this.findFriendshipByID();
                    case "up -u" -> this.updateUser();
                    case "del -u" -> this.deleteUser();
                    case "del -f" -> this.deleteFriendship();
                    case "all -u" -> this.displayUsers();
                    case "all -f" -> this.displayFriendships();
                    case "all -c" -> this.displayCommunities();
                    case "all -ch" -> this.displayChats();
                    case "all -m" -> this.displayMessages();
                    case "all -fr" -> this.displayRequests();
                    case "all -cN" -> this.displayCommunitiesNumber();
                    case "comm -a" -> this.displayMostActiveCommunity();
                    case "friendsofauser" -> this.friendshipsOfAUser();
                    case "friendsofauserm" -> this.friendshipsOfAUserMonth();
                    default -> System.out.println("unknown command <" + command + ">");
                }
                if (0 < exceptionCounter.get()) {
                    exceptionCounter.decrementAndGet();
                }
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
                if (10 <= exceptionCounter.incrementAndGet()) {
                    System.exit(1);
                }
            }
        }
    }
    private void friendshipsOfAUserMonth() throws SQLException, IOException {
        System.out.print("enter id:");
        final long id = Long.parseLong(this.reader.readLine());
        System.out.println("enter month:");
        final int month = Integer.parseInt(this.reader.readLine());
        List<Tuple<User, LocalDateTime>> friends = controller.friendshipsOfAUserMonth(id, month);

        for (Tuple<User, LocalDateTime> tuple: friends)
            System.out.println(tuple.left().getLastName() + "|" + tuple.left().getFirstName() + "|" + tuple.right());
    }

    private void friendshipsOfAUser() throws SQLException, IOException {
        System.out.print("enter id:");
        final long id = Long.parseLong(this.reader.readLine());
        List<Tuple<User, LocalDateTime>> friends = controller.friendshipsOfAUser(id);

        for (Tuple<User, LocalDateTime> tuple: friends)
            System.out.println(tuple.left().getLastName() + "|" + tuple.left().getFirstName() + "|" + tuple.right());
    }

    private void addUser() throws IOException, SQLException {
        System.out.print("firstname:");
        final String firstname = this.reader.readLine();
        System.out.print("lastname:");
        final String lastname = this.reader.readLine();
        this.userService.add(firstname, lastname);
    }

    private void addFriendship() throws IOException, SQLException {
        System.out.println("these are the users:");
        this.userService.getAll().forEach(System.out::println);
        System.out.print("enter first id:");
        final long first_id = Long.parseLong(this.reader.readLine());
        System.out.print("second first id:");
        final long second_id = Long.parseLong(this.reader.readLine());
        controller.add(first_id, second_id);
    }

    private void addChat() throws IOException, SQLException {
        System.out.print("enter the chat's name:");
        final String name = this.reader.readLine().strip();
        this.chatService.add(name);
    }

    private void addMessage() throws IOException, SQLException {
        System.out.print("enter sender id:");
        final long senderID = Long.parseLong(this.reader.readLine());
        System.out.print("enter chat id:");
        final int chatID = Integer.parseInt(this.reader.readLine());
        System.out.print("enter message body:");
        final String body = this.reader.readLine();
        System.out.print("are u replying?(y/n):");
        final String answer = this.reader.readLine();
        Integer replyID = null;
        if (Objects.equals(answer, "y")) {
            System.out.print("enter message id:");
            replyID = Integer.parseInt(this.reader.readLine());
        }
        this.messageService.add(senderID, chatID, body, replyID);
    }

    private void addRequest() throws IOException, SQLException {
        System.out.print("enter sender id:");
        final long senderID = Long.parseLong(this.reader.readLine());
        System.out.print("enter cited user id:");
        final long citedID = Long.parseLong(this.reader.readLine());
        this.requestService.add(senderID, citedID);
    }

    private void findUserByID() throws IOException, SQLException {
        System.out.print("enter id:");
        final long id = Long.parseLong(this.reader.readLine());
        final Optional<User> result = this.userService.findByID(id);
        result.ifPresent(System.out::println);
    }

    private void findFriendshipByID() throws IOException, SQLException {
        System.out.print("enter id:");
        final long id = Long.parseLong(this.reader.readLine());
        final @NotNull Optional<Friendship> result = this.controller.findByID(id);
        if (result.isPresent()) {
            System.out.println("found:" + result.get());
        } else {
            System.out.println("no user with <id: + " + id + ">");
        }
    }

    private void updateUser() throws IOException, SQLException {
        System.out.print("enter id:");
        final long id = Long.parseLong(this.reader.readLine());
        System.out.print("new firstname:");
        final String firstname = this.reader.readLine();
        System.out.print("new last name:");
        final String lastname = this.reader.readLine();
        final Optional<User> result = this.userService.update(id, firstname, lastname);
        result.ifPresent(System.out::println);
    }

    private void deleteUser() throws IOException, SQLException {
        System.out.print("enter id:");
        final long id = Long.parseLong(this.reader.readLine());
        final Optional<User> result = this.userService.delete(id);
        result.ifPresent(System.out::println);
    }

    private void deleteFriendship() throws IOException, SQLException {
        System.out.print("enter id:");
        final long id = Long.parseLong(this.reader.readLine());
        this.controller.delete(id);
    }

    private void displayUsers() throws SQLException {
        this.displayList(this.userService.getAll());
    }

    private void displayFriendships() throws SQLException {
        this.displayList(this.controller.getAll());
    }

    private void displayChats() throws SQLException {
        this.displayList(this.chatService.getAll());
    }

    private void displayMessages() throws IOException, SQLException {
        System.out.print("enter chat id:");
        final int chatID = Integer.parseInt(this.reader.readLine());
        this.displayList(this.chatController.getChatMessages(chatID));
    }

    private void displayCommunities() throws SQLException {
        this.displayList(this.controller.getCommunities());
    }

    private void displayCommunitiesNumber() throws SQLException {
        System.out.println("there are " + this.controller.getCommunitiesNumber() +
                " within meta's network");
    }

    private void displayMostActiveCommunity() throws SQLException {
        this.displayList(this.controller.getMostActiveCommunity());
    }

    private void displayRequests() throws IOException, SQLException {
        System.out.print("enter user id:");
        final long id = Long.parseLong(this.reader.readLine());
        System.out.print("send for or by?");
        final String answer = this.reader.readLine();
        if (answer.equals("for")) {
            this.displayList(this.requestService.getAllFor(id));
        } else if (answer.equals("by")) {
            this.displayList(this.requestService.getAllSendBy(id));
        } else {
            System.out.println("invalid answer!");
        }
    }

    private <T> void displayList(@NotNull final ArrayList<T> objects) {
        System.out.println("\n--------------------------------------");
        objects.forEach(System.out::println);
        System.out.println("--------------------------------------\n");
    }
}
