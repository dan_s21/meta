package controller;

import domain.model.Message;
import org.jetbrains.annotations.NotNull;
import service.ChatService;
import service.MessageService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public record ChatController(@NotNull ChatService chatService,
                             @NotNull MessageService messageService) {

    @NotNull
    public final ArrayList<Message> getChatMessages(final int chatID) throws SQLException {
        return  (ArrayList<Message>) this.messageService.getAll()
                .stream()
                .filter(message -> chatID == message.getChatID())
                .collect(Collectors.toList());
    }
}
