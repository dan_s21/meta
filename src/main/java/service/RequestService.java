package service;

import domain.model.FriendshipRequest;
import org.jetbrains.annotations.NotNull;
import repository.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

public record RequestService(@NotNull Repository<Integer, FriendshipRequest> repository) {

    public void add(final long senderID, final long citedID) throws SQLException {
        final FriendshipRequest request = new FriendshipRequest(senderID, citedID, FriendshipRequest.Status.PENDING);
        this.repository.add(request);
    }

    public void update(final int id, final FriendshipRequest.Status status) throws SQLException {
        final Optional<FriendshipRequest> result = this.repository.findByID(id);
        if (result.isPresent()) {
            final FriendshipRequest request = new FriendshipRequest(
                    result.get().getId(),
                    result.get().getSenderID(),
                    result.get().getCitedID(),
                    status
            );
            this.repository.update(request);
        }
    }

    @NotNull
    public Optional<FriendshipRequest> findByID(final int id) throws SQLException {
        return this.repository.findByID(id);
    }

    public void delete(final int id) throws SQLException {
        this.repository.delete(id);
    }

    public ArrayList<FriendshipRequest> getAllSendBy(final long senderID) throws SQLException {
        return (ArrayList<FriendshipRequest>) this.repository.getAll()
                .stream().filter(request -> request.getSenderID() == senderID)
                .collect(Collectors.toList());
    }

    public ArrayList<FriendshipRequest> getAllFor(final long citedID) throws SQLException {
        return (ArrayList<FriendshipRequest>) this.repository.getAll()
                .stream().filter(request -> request.getCitedID() == citedID)
                .collect(Collectors.toList());
    }
}
