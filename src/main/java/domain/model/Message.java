package domain.model;

import org.jetbrains.annotations.Nullable;

import java.time.LocalDateTime;

public class Message extends Entity<Integer> {

    private final long senderID;
    private final int chatID;
    private String body;
    private final LocalDateTime timestamp;
    @Nullable private final Integer replyID;

    public Message(final long senderID,
                   final int chatID,
                   final String body) {
        this(senderID, chatID, body, null);
    }

    public Message(final long senderID,
                   final int chatID,
                   final String body,
                   @Nullable final Integer replyID) {
        this.senderID = senderID;
        this.chatID = chatID;
        this.body = body;
        this.timestamp = LocalDateTime.now();
        this.replyID = replyID;
    }

    public Message(final int id,
                   final long senderID,
                   final int chatID,
                   final String body,
                   final LocalDateTime timestamp,
                   @Nullable final Integer replyID) {
        this.setId(id);
        this.senderID = senderID;
        this.chatID = chatID;
        this.body = body;
        this.timestamp = timestamp;
        this.replyID = replyID;
    }

    public final long getSenderID() {
        return senderID;
    }

    public final int getChatID() {
        return chatID;
    }

    public final String getBody() {
        return body;
    }

    public final void setBody(final String body) {
        this.body = body;
    }

    public final LocalDateTime getTimestamp() {
        return timestamp;
    }

    @Nullable
    public final Integer getReplyID() {
        return replyID;
    }

    @Override
    public String toString() {
        return  "#" + this.getId() + " from " + this.senderID + " " + this.body +
                (this.replyID != null ? " replied to: " + this.replyID : "");
    }
}
