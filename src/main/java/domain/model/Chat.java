package domain.model;

import org.jetbrains.annotations.NotNull;

public class Chat extends Entity<Integer> {

    private String name;

    public Chat(final String name) {
        this.name = name;
    }

    public Chat(final int id, final String name) {
        this.setId(id);
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final @NotNull String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "chat: #" + this.getId() + " name: " + this.name;
    }
}
